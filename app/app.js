angular.module('Preach', ['ui.router', 'firebase'])

.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {
  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'templates/home.html',
      controller: 'HomeController',
      resolve: {
        currentAuth: ['Auth', function(Auth) {
          return Auth.$requireSignIn();
        }]
      }
    })
    .state('login', {
      url: '/login',
      templateUrl: 'templates/auth.html',
      controller: 'AuthController'
    })
    .state('account', {
      url: '/account',
      templateUrl: 'templates/account.html',
      controller: 'AccountController',
      resolve: {
        currentAuth: ['Auth', function(Auth) {
          return Auth.$requireSignIn();
        }]
      }
    })
    .state('students', {
      url: '/students',
      templateUrl: 'templates/students.html',
      controller: 'StudentController',
      resolve: {
        currentAuth: ['Auth', function(Auth) {
          return Auth.$requireSignIn();
        }]
      }
    })
    .state('studentAdd', {
      url: '/new/student',
      templateUrl: 'templates/student.new.html',
      controller: 'StudentNewController',
      resolve: {
        currentAuth: ['Auth', function(Auth) {
          return Auth.$requireSignIn();
        }]
      }
    })
    .state('reports', {
      url: '/reports',
      templateUrl: 'templates/reports.html',
      controller: 'ReportController',
      resolve: {
        currentAuth: ['Auth', function(Auth) {
          return Auth.$requireSignIn();
        }]
      }
    })
    .state('report', {
      url: '/report/:reportId',
      templateUrl: 'templates/reportDetail.html',
      controller: 'ReportDetailController',
      resolve: {
        currentAuth: ['Auth', function(Auth) {
          return Auth.$requireSignIn();
        }]
      }
    })
    .state('student', {
      url: '/student/:studentId',
      templateUrl: 'templates/studentDetail.html',
      controller: 'StudentDetailController',
      resolve: {
        currentAuth: ['Auth', function(Auth) {
          return Auth.$requireSignIn();
        }]
      }
    })

  $urlRouterProvider.otherwise('/');
  $locationProvider.html5Mode(true);
}])

.run(['$rootScope', '$state', '$location', 'Auth',
  function($rootScope, $state, $location, Auth) {
    // ngMeta.init();
    // var progress = ngProgressFactory.createInstance();
    // var afterLogin;

    $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
      if (error === "AUTH_REQUIRED") {
        // $state.go('login', { toWhere: toState });
        $state.go('login');
        // progress.complete();
      }
    });

    // $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    //   progress.start();
    // });

    // $rootScope.$on('$stateChangeSuccess', function() {
    //   $rootScope.title = $state.current.meta.title;
    //   $rootScope.description = $state.current.meta.description;
    //   progress.complete();
    // });
  }
])
