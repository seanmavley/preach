angular.module("Preach")
.factory("DatabaseRef", function() {
  return firebase.database().ref();
});
