angular.module('Preach')
  .controller('AccountController', ['$state', '$scope', 'currentAuth',
    function($state, $scope, currentAuth) {
      $scope.authData = currentAuth;
    }
  ])
