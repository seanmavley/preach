angular.module('Preach')

.controller('HomeController', ['$scope', '$rootScope', 'currentAuth', 'DatabaseRef', '$firebaseArray',
  function($scope, $rootScope, currentAuth, DatabaseRef, $firebaseArray) {
    var studentArray = $firebaseArray(DatabaseRef.child('student').child(currentAuth.uid));

    studentArray.$loaded()
      .then(function(data) {
        $scope.students = data;
        $('select').material_select();
      })

    $scope.studentChange = function(studentId) {
      console.log(studentId);
      studentArray.$loaded()
        .then(function(data) {
          $scope.studentDetail = studentArray.$getRecord(studentId);
        })
    }

    $scope.studentReturns = function() {
      console.log($scope.student.returns);
    }
    
    $scope.addReport = function() {
      if (!currentAuth) {
        Materialize.toast('Log in first. You have not logged in. You cannot add report', 5000);
      } else if ($scope.reportData == null) {
        console.log('Empty report.');
        Materialize.toast('You cannot send empty report', 5000);
      } else {
        $scope.reportData.timestamp = new Date().getTime();
        console.log($scope.reportData);
        DatabaseRef.child('report')
          .child(currentAuth.uid)
          .set($scope.reportData,
            function(error) {
              if (!error) {
                $scope.reportData = '';
                Materialize.toast('Report Saved', 5000);
                $state.go('reportDetail');
              } else {
                Materialize.toast(error, 5000);
              }
            });
      }
    }
  }
])
