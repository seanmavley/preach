angular.module('Preach')
  .controller('StudentController', ['$scope', 'currentAuth', '$firebaseArray', 'DatabaseRef',
    function($scope, currentAuth, $firebaseArray, DatabaseRef) {
      var studentList = $firebaseArray(DatabaseRef.child('student').child(currentAuth.uid));
      studentList.$loaded()
        .then(function(data) {
          $scope.students = data;
        })
    }
  ])

.controller('StudentDetailController', ['$scope', 'currentAuth', '$firebaseObject', '$firebaseArray', '$stateParams', 'DatabaseRef',
  function($scope, currentAuth, $firebaseObject, $firebaseArray, $stateParams, DatabaseRef) {
    var StudentDetail = $firebaseArray(DatabaseRef.child('student').child(currentAuth.uid));
    StudentDetail.$loaded()
      .then(function(data) {
        $scope.student = StudentDetail.$getRecord($stateParams.studentId);
      })

    $scope.updateStudent = function() {
      StudentDetail.$save($scope.student)
        .then(function(data) {
          console.log('saved happen');
          Materialize.toast('Student details updated successfully', 5000);
        })
        .catch(function(error) {
          console.log(error.message);
          Materialize.toast(error.message, 5000);
        })
    }
  }
])

.controller('StudentNewController', ['$scope', '$state', 'currentAuth', 'DatabaseRef', '$firebaseArray',
  function($scope, $state, currentAuth, DatabaseRef, $firebaseArray) {
    $scope.addStudent = function() {
      if (!$scope.student) {
        Materialize.toast('Form is empty. Fill form first then submit');
      } else {
        console.log($scope.student);
        $scope.student.timestamp = new Date().getTime();
        var newStudent = $firebaseArray(DatabaseRef.child('student').child(currentAuth.uid));
        newStudent.$add($scope.student)
          .then(function(savedStudent) {
            console.log(savedStudent.key);
            Materialize.toast('Student saved successfully', 5000);
            $state.go('studentDetail', { studentId: savedStudent.key });
          })
          .catch(function(error) {
            console.log(error.message, error.reason);
            Materialize.toast(error.message, 5000);
          })
      }
    }
  }
])
