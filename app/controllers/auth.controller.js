angular.module('Preach')
  .controller('AuthController', ['$scope', '$stateParams', 'Auth', '$state', 'DatabaseRef',
    function($scope, $stateParams, Auth, $state, DatabaseRef) {
      // Social Auths Only
      // GOOGLE AUTH
      $scope.googleAuth = function() {
        var provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope('https://www.googleapis.com/auth/plus.login');

        Auth.$signInWithPopup(provider)
          .then(function(firebaseUser) {
            console.log('Logged in');
            Materialize.toast('Logged in with Google successfully', 5000);
            $state.go('account');
          })
          .catch(function(error) {
            console.log(error.message, error.reason);
            Materialize.toast(error.message, 5000);
          })
      }

      // FACEBOOK AUTH
      $scope.facebookAuth = function() {
        var provider = new firebase.auth.FacebookAuthProvider();
        provider.addScope('email');

        Auth.$signInWithPopup(provider)
          .then(function(firebaseUser) {
            console.log('Logged in success');
            Materialize.toast('Logged in with Google successfully', 5000);
            $state.go('home');
          })
          .catch(function(error) {
            console.log(error.message, error.reason);
            Materialize.toast(error.message, 5000);
          })
      }
    }
  ])
